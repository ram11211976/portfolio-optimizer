** The project is to do portfolio optimization for bonds given the specific constraints of each account.

Specific constraints looks like

1. Maturity bucket constraints
	ex:              Min	Max
		 1 -5   yr   10%	20%
		 5 - 10 yr   15%
		 10-15	     25%
		 15-25		 5%
		 
2. Min cash 	5% of account value
3. Duartion 	Min - 10	Max - 15
4. MMD spread	5 bps (offers with more than MMD spread of 5 bps will be rejected. )
5. Accoutns priority ( Fill/priortize the accounts by  Max deviation , Oldest account )
6. Block size : 30,000 ( The minimum blocksize to be bought across accounts.)
7. No. of unique issuers : 15 min 25 max. (Diversifies risk)
