package com.qpm.am.model;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.qpm.am.model.Utils.distinctByKey;

public class OffersReader {


 public static void main(String[] args){
     try {
         //Reader reader = Files.newBufferedReader(Paths.get("C:\\ganesha\\resources\\Offers.csv"));
         List<Offer> beans = new CsvToBeanBuilder(new FileReader("C:\\\\ganesha\\\\resources\\\\Offers.csv"))
                 .withType(Offer.class).build().parse();
         for(Offer offer: beans) {
             if (offer.getQuantityAsk() != null) {
                 String[] askQuantityMin = offer.getQuantityAsk().split("\\(|\\)");
                 if (askQuantityMin.length == 2) {
                     offer.setOfferSize(Integer.parseInt(askQuantityMin[0].trim().replace(",", "")) * 1000);
                     offer.setMinimum(Integer.parseInt(askQuantityMin[1].trim().replace(",", "")) * 1000);
                 }
             }
         }
         List<Offer> distinctStates = beans.stream().filter(distinctByKey(p -> p.getState())).collect(Collectors.toList());
        System.out.println("list is "+ beans);
     } catch (IOException e) {
         e.printStackTrace();
     }

 }


}
