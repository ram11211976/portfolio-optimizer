//
// Copyright: $$copyright
//
// File:      $${file}
//
// Purpose: Demonstrates how to solve the problem
//
// minimize 3*x0 + 5*x1 + x2 + x3
// such that
//          3*x0 + 2*x1        +   x3 = 30,
//          2*x0 + 3*x1 +   x2 +   x3 > 15,
//                      + 3*x2 + 2*x3 < 25
// and
//          x0,x1,x2> 0,
//          0 < x3 < 10
//
//TAG:begin-lo1
package com.qpm.am.model;
//TAG:begin-import

import com.opencsv.bean.CsvToBeanBuilder;
import mosek.fusion.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import static com.qpm.am.model.Utils.distinctByKey;
//TAG:end-import

public class PortfolioOptimizer
{
    public static void main(String[] args)
      throws SolutionError
    {

        List<Offer> offers=null;
        List<Offer> distinctStates = null;

        double[][] maturityMixes = {
                {2018,5,10},
                {2019,10,25},
                {2020,10,25},
                {2021,10,25},
                {2022,10,25},
                {2023,10,40}
        };

        double [][] stateValues=null;

        try {
            offers = new CsvToBeanBuilder(new FileReader("C:\\\\ganesha\\\\resources\\\\Offers.csv"))
                    .withType(Offer.class).build().parse();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for(Offer offer: offers){
            if (offer.getQuantityAsk() != null) {
                String[] askQuantityMin = offer.getQuantityAsk().split("\\(|\\)");
                if (askQuantityMin.length == 2) {
                    offer.setOfferSize(Integer.parseInt(askQuantityMin[0].trim().replace(",", "")) * 1000);
                    offer.setMinimum(Integer.parseInt(askQuantityMin[1].trim().replace(",", "")) * 1000);
                }
            }
        }
        offers = offers.stream().filter(offer -> offer.getAskPrice()!=null).collect(Collectors.toList());
        distinctStates = offers.stream().filter(distinctByKey(p -> p.getState())).collect(Collectors.toList());

        int minVar =offers.size();
        int multVar =offers.size();

        //int numCon =  distinctStates.size() + principalAmountConSize + multipleConSize + minimumConSize ;
        int numVar = offers.size() + minVar + multVar;

        double[] principalAmountCon = new double[numVar];
        double[][] stateCon = new double[distinctStates.size()][numVar];
        double[][] maturityCon = new double[maturityMixes.length][numVar];
        double[][] maturityMaxCon = new double[maturityMixes.length][numVar];

        for (int i=0;i<offers.size();i++){
            principalAmountCon[i]=offers.get(i).getAskPrice().doubleValue()/100;
        }

        for (int i=0;i< distinctStates.size();i++) {
            for (int j = 0; j < offers.size(); j++) {
                if (distinctStates.get(i).getState().equals(offers.get(j).getState()))
                    stateCon[i][j] = offers.get(i).getAskPrice().doubleValue() / 100 * (1 - .15);
                else
                    stateCon[i][j] = offers.get(i).getAskPrice().doubleValue() / 100 * (-.15);
            }
        }

        for (int i=0;i< maturityMixes.length;i++) {
            for (int j = 0; j < offers.size(); j++) {
                LocalDate localDate = offers.get(j).getMaturityDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if (maturityMixes[i][0] ==  localDate.getYear())
                    maturityCon[i][j] = (offers.get(i).getAskPrice().doubleValue() / 100) * (1 - (maturityMixes[i][1]/100));
                else
                    maturityCon[i][j] = (offers.get(i).getAskPrice().doubleValue() / 100) * ((maturityMixes[i][1]/100));
            }
        }


        for (int i=0;i< maturityMixes.length;i++) {
            for (int j = 0; j < offers.size(); j++) {
                LocalDate localDate = offers.get(j).getMaturityDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if (maturityMixes[i][0] ==  localDate.getYear())
                    maturityMaxCon[i][j] = (offers.get(i).getAskPrice().doubleValue() / 100) * (1 - (maturityMixes[i][2]/100));
                else
                    maturityMaxCon[i][j] = (offers.get(i).getAskPrice().doubleValue() / 100) * ((maturityMixes[i][2]/100));
            }
        }


        double minCon[][] = new double[offers.size()][numVar];
        double[] minConY = new  double[numVar];
        for (int i=0;i<offers.size();i++){
            for(int j=0;j<offers.size();j++){
                if(offers.get(i).getCusip().equals(offers.get(j).getCusip())){
                    minCon[i][j] = 1  ;
                    minCon[i][j+offers.size()]= - offers.get(i).getMinimum();
                }
            }
            minConY[i]= offers.get(i).getMinimum();
        }

        double multCon[][] = new double[offers.size()][numVar];
        for (int i=0;i<offers.size();i++){
            for(int j=0;j<offers.size();j++){
                if(offers.get(i).getCusip().equals(offers.get(j).getCusip())){
                    multCon[i][j] = 1  ;
                    multCon[i][j+offers.size()*2]= -5000;
                }
            }
        }

        //double[][] allConCoEff = Stream.of(principalAmountCon,stateCon,minCon,multCon).flatMap(Stream::of)
          //      .toArray(double[][]::new);


//TAG:begin-define-data
        /*double[][] A =
            { { 3.0, 2.0, 0.0, 1.0 },
              { 2.0, 3.0, 1.0, 1.0 },
              { 0.0, 0.0, 3.0, 2.0 } };
        double[] c = { 3.0, 5.0, 1.0, 1.0  };*/
        double[] c = new double[numVar];

        for ( int i=0;i<offers.size();i++){
            c[i]=offers.get(i).getAskYieldToWorst().doubleValue();
        }

//TAG:end-define-data
        
        // Create a model with the name 'lo1'
//TAG:begin-create-model
        try(Model M = new Model("portfolioOptimizer"))
//TAG:end-create-model
        {
//TAG:begin-create-variable
          // Create variable 'x' of length 4
          Variable x = M.variable("x", numVar , Domain.integral(Domain.greaterThan(0)));

          Variable y = M.variable("y", numVar , Domain.binary());

          for ( int i=0;i<offers.size();i++) {
              M.constraint(x.index(i), Domain.lessThan(offers.get(i).getOfferSize()));
          }

//TAG:end-create-variable

//TAG:begin-create-constraints
          // Create three constraints
          M.constraint("principalAmountConstraint", Expr.dot(principalAmountCon, x), Domain.lessThan(200000));

          for(int i=0;i<minCon.length;i++) {
              M.constraint(Expr.sub(x.index(i), Expr.mul(minConY[i], y.index(i))), Domain.greaterThan(0));
              M.constraint(Expr.sub(x.index(i), Expr.mul(offers.get(i).getOfferSize(), y.index(i))), Domain.lessThan(0));
          }


          for(int i=0;i<distinctStates.size();i++)
              M.constraint("state " + i, Expr.dot(stateCon[i], x), Domain.lessThan(0));


          for(int i=0;i<maturityMixes.length;i++) {
              M.constraint("Maturity Min " + i, Expr.dot(maturityCon[i], x), Domain.greaterThan(0));
              //M.constraint("Maturity Max " + i, Expr.dot(maturityMaxCon[i], x), Domain.lessThan(0));
          }

           for(int i=0;i<multCon.length;i++)
             M.constraint("multCon " + i, Expr.dot(multCon[i], x), Domain.equalsTo(0));

//TAG:end-create-constraints

//TAG:begin-set-objective
          // Set the objective function to (c^t * x)
          M.objective("obj", ObjectiveSense.Maximize, Expr.dot(c, x));
//TAG:end-set-objective
        
          // Solve the problem
//TAG:begin-solve
          M.solve();
//TAG:end-solve


//TAG:begin-get-solution
          // Get the solution values
          double[] sol = x.level();
          for (int i=0;i<sol.length;i++){
              if (sol[i]>0)
                  System.out.println( "i level  " +i + " solution "
                          +  offers.get(i%offers.size()).getCusip() + " State "
                          + offers.get(i%offers.size()).getState() + "    "+ sol[i]);
          }

            double[] min = y.level();
            for (int i=0;i<min.length;i++){
                if (min[i]>0)
                    System.out.println("solution Y " + i + "  :  " + offers.get(i%offers.size()).getCusip() + " " + min[i]);
            }

          //System.out.printf("[x0,x1,x2,x3] = [%e, %e, %e, %e]\n",sol[0],sol[1],sol[2],sol[3]);
//TAG:end-get-solution
        }
    }
}

//TAG:end-lo1
