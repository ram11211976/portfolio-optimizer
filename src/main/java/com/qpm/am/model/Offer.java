package com.qpm.am.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

import java.math.BigDecimal;
import java.util.Date;

public class Offer {

    @CsvBindByName
    String Cusip;

    @CsvBindByName
    String State;


    @CsvBindByName
    String Description;

    @CsvBindByName
    String Coupon;

    @CsvBindByName(column = "Maturity Date")
    @CsvDate("MM/dd/yyyy")
    Date MaturityDate;

    @CsvBindByName(column = "Moody's Rating")
    String MoodyRating;

    @CsvBindByName(column = "S&P Rating")
    String SPRating;

    @CsvBindByName(column = "Moody's Underlying Rating")
    String MoodysULRatings;

    @CsvBindByName(column = "S&P Underlying Rating")
    String SPULRatings;

    @CsvBindByName(column = "Price Bid")
    String BidPrice;

    @CsvBindByName(column = "Price Ask")
    BigDecimal askPrice;

    @CsvBindByName(column = "Yield Bid")
    String bidYield;

    @CsvBindByName(column = "Ask Yield to Worst")
    BigDecimal AskYieldToWorst;

    @CsvBindByName(column = "Ask Yield to Maturity")
    String AskYielToMaturity;

    @CsvBindByName(column = "Quantity Bid(min)")
    String QuantityBid;

    @CsvBindByName(column = "Quantity Ask(min)")
    String QuantityAsk;

    @CsvBindByName(column = "Attributes")
    String Attributes;

    int offerSize;

    int minimum;

    public int getOfferSize() {
        return offerSize;
    }

    public void setOfferSize(int offerSize) {
        this.offerSize = offerSize;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public String getCusip() {
        return Cusip;
    }

    public void setCusip(String cusip) {
        Cusip = cusip;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCoupon() {
        return Coupon;
    }

    public void setCoupon(String coupon) {
        Coupon = coupon;
    }

    public Date getMaturityDate() {
        return MaturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        MaturityDate = maturityDate;
    }

    public String getMoodyRating() {
        return MoodyRating;
    }

    public void setMoodyRating(String moodyRating) {
        MoodyRating = moodyRating;
    }

    public String getSPRating() {
        return SPRating;
    }

    public void setSPRating(String SPRating) {
        this.SPRating = SPRating;
    }

    public String getMoodysULRatings() {
        return MoodysULRatings;
    }

    public void setMoodysULRatings(String moodysULRatings) {
        MoodysULRatings = moodysULRatings;
    }

    public String getSPULRatings() {
        return SPULRatings;
    }

    public void setSPULRatings(String SPULRatings) {
        this.SPULRatings = SPULRatings;
    }

    public String getBidPrice() {
        return BidPrice;
    }

    public void setBidPrice(String bidPrice) {
        BidPrice = bidPrice;
    }

    public BigDecimal getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(BigDecimal askPrice) {
        this.askPrice = askPrice;
    }

    public String getBidYield() {
        return bidYield;
    }

    public void setBidYield(String bidYield) {
        this.bidYield = bidYield;
    }

    public BigDecimal getAskYieldToWorst() {
        return AskYieldToWorst;
    }

    public void setAskYieldToWorst(BigDecimal askYieldToWorst) {
        AskYieldToWorst = askYieldToWorst;
    }

    public String getAskYielToMaturity() {
        return AskYielToMaturity;
    }

    public void setAskYielToMaturity(String askYielToMaturity) {
        AskYielToMaturity = askYielToMaturity;
    }

    public String getQuantityBid() {
        return QuantityBid;
    }

    public void setQuantityBid(String quantityBid) {
        QuantityBid = quantityBid;
    }

    public String getQuantityAsk() {
        return QuantityAsk;
    }

    public void setQuantityAsk(String quantityAsk) {
        QuantityAsk = quantityAsk;
    }

    public String getAttributes() {
        return Attributes;
    }

    public void setAttributes(String attributes) {
        Attributes = attributes;
    }


    @Override
    public String toString() {
        return "Offer{" +
                "Cusip='" + Cusip + '\'' +
                ", State='" + State + '\'' +
                ", Description='" + Description + '\'' +
                ", Coupon='" + Coupon + '\'' +
                ", MaturityDate=" + MaturityDate +
                ", MoodyRating='" + MoodyRating + '\'' +
                ", SPRating='" + SPRating + '\'' +
                ", MoodysULRatings='" + MoodysULRatings + '\'' +
                ", SPULRatings='" + SPULRatings + '\'' +
                ", BidPrice='" + BidPrice + '\'' +
                ", askPrice=" + askPrice +
                ", bidYield='" + bidYield + '\'' +
                ", AskYieldToWorst=" + AskYieldToWorst +
                ", AskYielToMaturity='" + AskYielToMaturity + '\'' +
                ", QuantityBid='" + QuantityBid + '\'' +
                ", QuantityAsk='" + QuantityAsk + '\'' +
                ", Attributes='" + Attributes + '\'' +
                '}';
    }
}
