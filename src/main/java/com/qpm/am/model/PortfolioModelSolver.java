/*
   Copyright : Copyright (c) MOSEK ApS, Denmark. All rights reserved.

   File :      milo1.java

   Purpose :   Demonstrates how to solve a small mixed
               integer linear optimization problem using the MOSEK Java API.
*/
package com.qpm.am.model;

import com.opencsv.bean.CsvToBeanBuilder;
import mosek.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.Exception;
import java.util.List;
import java.util.stream.Collectors;

import static com.qpm.am.model.Utils.distinctByKey;

public class PortfolioModelSolver {

  public static void main (String[] args) {
    List<Offer> offers=null;
    List<Offer> distinctStates = null;
    int numcon=0;
    int numvar=0;

      double [][] stateValues=null;
      try {
        offers = new CsvToBeanBuilder(new FileReader("C:\\\\ganesha\\\\resources\\\\Offers.csv"))
              .withType(Offer.class).build().parse();
        for(Offer offer: offers){
            if (offer.getQuantityAsk() != null) {
                String[] askQuantityMin = offer.getQuantityAsk().split("\\(|\\)");
                if (askQuantityMin.length == 2) {
                    offer.setOfferSize(Integer.parseInt(askQuantityMin[0].trim().replace(",", "")) * 1000);
                    offer.setMinimum(Integer.parseInt(askQuantityMin[1].trim().replace(",", "")) * 1000);
                }
            }
        }
        offers = offers.stream().filter(offer -> offer.getAskPrice()!=null).collect(Collectors.toList());
        distinctStates = offers.stream().filter(distinctByKey(p -> p.getState())).collect(Collectors.toList());
        stateValues = new double[distinctStates.size()+1][offers.size()];

        numcon = 1 + distinctStates.size() + offers.size()-1;
        numvar = offers.size()*3-2;

        for (int i=0;i<distinctStates.size()+1;i++){
            for(int j=0;j<offers.size();j++){
                if(i==0)
                    stateValues[i][j]= offers.get(j).getAskPrice().doubleValue() / 100 ;
                else if (j< offers.size()){
                    if (offers.get(j).getState().equals(distinctStates.get(i-1).getState()))
                        stateValues[i][j] = offers.get(i).getAskPrice().doubleValue() / 100 * (1 - .15);
                    else
                        stateValues[i][j] = offers.get(i).getAskPrice().doubleValue() / 100 * (-.15);
                }
            }
        }
        int currentPos = distinctStates.size();
        for (int i=0;i<offers.size();i++){
            for (int j=0;j<offers.size();j++){
                    if (offers.get(i).getCusip().equals(offers.get(j).getCusip())) {
                        stateValues[currentPos + i][j] = 1;
                        stateValues[currentPos+i][j+offers.size()-1]= -offers.get(j).getMinimum();
                        stateValues[currentPos+i][j+(offers.size()*2)-1]= -5000;
                    }
                    else
                        stateValues[currentPos + i][j] = 0;
            }
        }

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }catch (Exception e){
          e.printStackTrace();
      }


      // Since the value infinity is never used, we define
    // 'infinity' symbolic purposes only
    double infinity = 0;

      boundkey[] bkc
              = new boundkey[numcon];
      double[] blc = new double[numcon];
      double[] buc = new double[numcon];

      for (int i=0;i<numcon;i++){
          if(i <= distinctStates.size()) {
              bkc[i] = boundkey.up;
              blc[i] = -infinity;
              if (i == 0)
                  buc[i] = 200000;
              else
                  buc[i] = 0;
          }else{

          }
      }

    /*boundkey[] bkc
      = { boundkey.up, boundkey.up };
    double[] blc = { -infinity, -infinity};
    double[] buc = { 200000.0,0 };*/



      boundkey[] bkx = new boundkey[offers.size()];
      double[] blx = new double[offers.size()];
      double[] bux = new double[offers.size()];


      double[] c = new double[offers.size()];


      int[][] asub    = new int[numvar][numcon];
      double[][] aval = new double[numvar][numcon];

      for(int j=0;j<numvar;j++) {
          for (int i = 0; i < numcon; i++) {
              if(i==0) {
                  bkx[j] = boundkey.ra;
                  blx[j] = 0;
                  bux[j] = offers.get(j).getOfferSize();
                  c[j] = offers.get(j).getAskYieldToWorst() != null ? offers.get(j).getAskYieldToWorst().doubleValue() : 0;
              }
              asub[j][i] = i;
              aval[j][i] = stateValues[i][j];
          }
      }


    int[] ptrb = { 0, 2 };
    int[] ptre = { 2, 4 };

    double[] xx  = new double[numvar];

    try (Env  env  = new Env();
         Task task = new Task(env, 0, 0)) {
      // Directs the log task stream to the user specified
      // method task_msg_obj.stream
      task.set_Stream(
        streamtype.log,
        new Stream()
      { public void stream(String msg) { System.out.print(msg); }});
      task.set_ItgSolutionCallback(
      new ItgSolutionCallback() {
        public void callback(double[] xx) {
          System.out.print("New integer solution: ");
          for (double v : xx) System.out.print("" + v + " ");
          System.out.println("");
        }
      });
      /* Append 'numcon' empty constraints.
      The constraints will initially have no bounds. */
      task.appendcons(numcon);

      /* Append 'numvar' variables.
      The variables will initially be fixed at zero (x=0). */
      task.appendvars(numvar);

      for (int j = 0; j < numvar; ++j) {
        /* Set the linear term c_j in the objective.*/
        System.out.println(" j is " + j);
        task.putcj(j, c[j]);
        /* Set the bounds on variable j.
           blx[j] <= x_j <= bux[j] */
        task.putvarbound(j, bkx[j], blx[j], bux[j]);
        /* Input column j of A */
        task.putacol(j,                     /* Variable (column) index.*/
                     asub[j],               /* Row index of non-zeros in column j.*/
                     aval[j]);              /* Non-zero Values of column j. */
      }
      /* Set the bounds on constraints.
       for i=1, ...,numcon : blc[i] <= constraint i <= buc[i] */
      for (int i = 0; i < numcon; ++i)
        task.putconbound(i, bkc[i], blc[i], buc[i]);

      /* Specify integer variables. */
      for (int j = 0; j < numvar; ++j)
        task.putvartype(j, variabletype.type_int);

      /* Set max solution time */
      task.putdouparam(dparam.mio_max_time, 60.0);


      /* A maximization problem */
      task.putobjsense(objsense.maximize);
      /* Solve the problem */
      try {
        task.optimize();
      } catch (Warning e) {
        System.out.println (" Mosek warning:");
        System.out.println (e.toString ());
      }

      // Print a summary containing information
      //   about the solution for debugging purposes
      task.solutionsummary(streamtype.msg);
      task.getxx(soltype.itg, // Integer solution.
                 xx);
      solsta solsta[] = new solsta[1];
      /* Get status information about the solution */
      task.getsolsta(soltype.itg, solsta);

      switch (solsta[0]) {
        case integer_optimal:
        case near_integer_optimal:
          System.out.println("Optimal solution\n");
          for (int j = 0; j < numvar; ++j)
              if(xx[j]>0)
                System.out.println ("x[" + j + "]:" + xx[j]);
          break;
        case prim_feas:
          System.out.println("Feasible solution\n");
          for (int j = 0; j < numvar; ++j) {
              if(xx[j]>0)
                System.out.println("x[" + j + "]:" + xx[j]);
          }
          break;

        case unknown:
          prosta prosta[] = new prosta[1];
          task.getprosta(soltype.itg, prosta);
          switch (prosta[0]) {
            case prim_infeas_or_unbounded:
              System.out.println("Problem status Infeasible or unbounded");
              break;
            case prim_infeas:
              System.out.println("Problem status Infeasible.");
              break;
            case unknown:
              System.out.println("Problem status unknown.");
              break;
            default:
              System.out.println("Other problem status.");
              break;
          }
          break;
        default:
          System.out.println("Other solution status");
          break;
      }
    }
    catch (mosek.Exception e) {
      System.out.println ("An error or warning was encountered");
      System.out.println (e.getMessage ());
      throw e;
    }
  }
}
